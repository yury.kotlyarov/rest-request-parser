module RequestParser where

import Text.ParserCombinators.Parsec

data Request  = Request Verb Resource deriving (Eq, Show)
data Verb     = LIST | POST deriving (Eq, Show, Read)
data Resource = Collection Name | Element Name Id deriving (Eq, Show)
data Name     = Name String deriving (Eq, Show)
data Id       = Id Int deriving (Eq, Show)

parseVerb :: Parser Verb
parseVerb = (string "LIST" <|> string "POST") >>= (return . read)

parseResourceName :: Parser Name
parseResourceName = do
  char '/'
  r <- many1 alphaNum
  return $ Name r

parseResourceId :: Parser Id
parseResourceId = do
  char '/'
  many1 digit >>= (return . Id . read)

parseElement :: Parser Resource
parseElement = do
  name <- parseResourceName
  id <-   parseResourceId
  return $ Element name id

parseCollection :: Parser Resource
parseCollection = do
  name <- parseResourceName
  return $ Collection name

parseResource = do
  try parseElement <|> parseCollection

parseRequest :: Parser Request
parseRequest = do
  v <- parseVerb
  space
  r <- parseResource
  return $ Request v r
