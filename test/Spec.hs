import Test.Hspec
import Test.Hspec.Parsec
import Text.Parsec
import Text.Parsec.Error

import RequestParser

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "parseVerb" $ do
    let parseVerb' = parse parseVerb ""
    
    it "parses LIST verb" $ do
      parseVerb' "LIST" `shouldParse` LIST

    it "parses POST verb" $ do
      parseVerb' "POST" `shouldParse` POST

    it "does not parse GET verb" $ do
      parseVerb' `shouldFailOn` "GET"

  describe "parseResourceName" $ do
    let parseResourceName' = parse parseResourceName ""
    
    it "parses '/vpc' resource name" $ do
      parseResourceName' "/vpc" `shouldParse` Name "vpc"

    it "parses '/vpc/1' resource name" $ do
      parseResourceName' "/vpc/1" `shouldParse` Name "vpc"

  describe "parseResourceId" $ do
    let parseResourceId' = parse parseResourceId ""
    
    it "parses '/42' id" $ do
      parseResourceId' "/42" `shouldParse` Id 42

    it "fails on empty strings" $ do
      parseResourceId' `shouldFailOn` ""

  describe "parseCollection" $ do
    let parseCollection' = parse parseCollection ""
    
    it "parses '/vpc' collection" $ do
      parseCollection' "/vpc" `shouldParse` Collection (Name "vpc")

  describe "parseElement" $ do
    let parseElement' = parse parseElement ""
    
    it "parses '/vpc/1' element" $ do
      parseElement' "/vpc/1" `shouldParse` Element (Name "vpc") (Id 1)

  describe "parseResource" $ do
    let parseRequest' = parse parseRequest ""
    
    it "parses 'LIST /vpc'" $ do
      parseRequest' "LIST /vpc" `shouldParse` Request LIST (Collection (Name "vpc"))

    it "parses 'POST /vpc/1'" $ do
      parseRequest' "POST /vpc/1" `shouldParse` Request POST (Element (Name "vpc") (Id 1))
