module Main where

import RequestParser
import Text.Parsec

main :: IO ()
main = do
  putStrLn $ show $ parse parseRequest "" "LIST /vpc"
  putStrLn $ show $ parse parseRequest "" "POST /vpc/1"
